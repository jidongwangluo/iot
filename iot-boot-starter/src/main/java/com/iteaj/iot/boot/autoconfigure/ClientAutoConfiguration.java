package com.iteaj.iot.boot.autoconfigure;

import com.iteaj.iot.IotThreadManager;
import com.iteaj.iot.client.ClientProperties;
import com.iteaj.iot.client.IotClientBootstrap;
import com.iteaj.iot.boot.condition.ConditionalOnIotClient;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Configuration(proxyBeanMethods = false)
@ConditionalOnIotClient
public class ClientAutoConfiguration {

    @Configuration
    @EnableConfigurationProperties(ClientProperties.class)
    public static class IotClientAutoConfiguration extends IotClientBootstrap {

        public IotClientAutoConfiguration(IotThreadManager threadManager, ClientProperties properties) {
            super(threadManager, properties);
        }
    }
}
