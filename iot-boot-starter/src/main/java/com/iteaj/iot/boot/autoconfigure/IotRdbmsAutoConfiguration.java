package com.iteaj.iot.boot.autoconfigure;

import com.iteaj.iot.tools.db.RdbmsAutoConfiguration;
import com.iteaj.iot.tools.db.rdbms.RdbmsHandle;
import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.context.annotation.Configuration;

@Configuration
@ConditionalOnBean(RdbmsHandle.class)
public class IotRdbmsAutoConfiguration extends RdbmsAutoConfiguration {

}
