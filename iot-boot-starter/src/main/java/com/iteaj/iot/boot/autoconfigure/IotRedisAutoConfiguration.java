package com.iteaj.iot.boot.autoconfigure;

import com.iteaj.iot.redis.IotRedisConfiguration;
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.context.annotation.Configuration;

@Configuration(proxyBeanMethods = false)
@ConditionalOnClass(name = "com.iteaj.iot.redis.IotRedisConfiguration")
public class IotRedisAutoConfiguration {

    @Configuration
    public static class RedisAutoConfiguration extends IotRedisConfiguration { }
}
