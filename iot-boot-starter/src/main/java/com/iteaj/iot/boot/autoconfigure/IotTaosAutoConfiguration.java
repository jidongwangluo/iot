package com.iteaj.iot.boot.autoconfigure;

import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.context.annotation.Configuration;

@Configuration(proxyBeanMethods = false)
@ConditionalOnClass(name = "com.iteaj.iot.taos.IotTaosAutoConfiguration")
public class IotTaosAutoConfiguration {

    @Configuration
    public static class TaosAutoConfiguration extends com.iteaj.iot.taos.IotTaosAutoConfiguration { }
}
