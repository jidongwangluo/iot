package com.iteaj.iot.boot.condition;

import org.springframework.context.annotation.Conditional;

import java.lang.annotation.*;

@Target({ ElementType.TYPE, ElementType.METHOD })
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Conditional(ConditionalOnIot.class)
public @interface ConditionalOnIotServer {

    String value() default "com.iteaj.iot.server.ServerComponentFactory";
}
