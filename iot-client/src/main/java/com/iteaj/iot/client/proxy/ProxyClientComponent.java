package com.iteaj.iot.client.proxy;

import com.iteaj.iot.ProtocolType;
import com.iteaj.iot.client.ClientConnectProperties;
import com.iteaj.iot.client.ClientProperties;
import com.iteaj.iot.client.TcpSocketClient;
import com.iteaj.iot.client.component.TcpClientComponent;
import com.iteaj.iot.client.protocol.ServerInitiativeProtocol;
import com.iteaj.iot.proxy.ProxyClientMessage;
import com.iteaj.iot.utils.UniqueIdGen;

public class ProxyClientComponent extends TcpClientComponent<ProxyClientMessage> {

    public static final String ProxyClientPrefix = "Proxy:SN:";
    public static final String DefaultProxyClient = "Default:Proxy:SN";
    public ProxyClientComponent(ClientProperties.ClientProxyConnectProperties config) {
        super(config);
        if(config.getDeviceSn() == null) {
            config.setDeviceSn(DefaultProxyClient);
        }
    }

    @Override
    public TcpSocketClient createNewClient(ClientConnectProperties config) {
        ClientProperties.ClientProxyConnectProperties clientProxy;
        if(config instanceof ClientProperties.ClientProxyConnectProperties) {
            clientProxy = (ClientProperties.ClientProxyConnectProperties) config;
            if(clientProxy.getDeviceSn() == null) {
                clientProxy.setDeviceSn(ProxyClientPrefix + UniqueIdGen.deviceSn());
            }
        } else {
            throw new IllegalArgumentException("请使用代理客户端配置对象[ClientProperties.ClientProxy]");
        }

        return new ProxyClient(this, clientProxy);
    }

    @Override
    public String getName() {
        return "代理客户端";
    }

    @Override
    public String getDesc() {
        return "用于客户端对设备的代理操作";
    }

    @Override
    protected ServerInitiativeProtocol instanceProtocol(ProxyClientMessage message, ProtocolType type) {
        return null;
    }
}
