package com.iteaj.iot.proxy;

public interface AppTradeType {

    String desc();

    Enum getTradeType();
}
