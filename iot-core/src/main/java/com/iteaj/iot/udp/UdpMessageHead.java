package com.iteaj.iot.udp;

import com.iteaj.iot.Message;
import com.iteaj.iot.ProtocolType;
import com.iteaj.iot.message.DefaultMessageHead;

public class UdpMessageHead extends DefaultMessageHead {

    public UdpMessageHead() {
        super(Message.EMPTY);
    }

    public UdpMessageHead(byte[] message) {
        super(message);
    }

    public UdpMessageHead(ProtocolType type) {
        super(null, null, type);
    }

    public UdpMessageHead(String equipCode, String messageId, ProtocolType type) {
        super(equipCode, messageId, type);
    }

}
