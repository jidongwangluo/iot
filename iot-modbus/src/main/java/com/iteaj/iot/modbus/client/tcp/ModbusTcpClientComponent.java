package com.iteaj.iot.modbus.client.tcp;

import com.iteaj.iot.ProtocolType;
import com.iteaj.iot.SocketMessage;
import com.iteaj.iot.client.*;
import com.iteaj.iot.client.component.TcpClientComponent;
import com.iteaj.iot.client.protocol.ServerInitiativeProtocol;
import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;

public class ModbusTcpClientComponent<M extends ModbusTcpClientMessage> extends TcpClientComponent<M> {

    private static final String DESC = "基于Modbus Tcp协议的Iot客户端实现";

    public ModbusTcpClientComponent() { }

    public ModbusTcpClientComponent(ClientConnectProperties config) {
        super(config);
    }

    public ModbusTcpClientComponent(ClientConnectProperties config, MultiClientManager clientManager) {
        super(config, clientManager);
    }

    @Override
    public TcpSocketClient createNewClient(ClientConnectProperties config) {
        return new ModbusTcpClient(this, config);
    }

    @Override
    public SocketMessage doTcpDecode(ChannelHandlerContext ctx, ByteBuf in) {
        SocketMessage socketMessage = super.doTcpDecode(ctx, in);
        if(socketMessage instanceof ModbusTcpClientMessage) {
            // 使用Channel id作为设备编号
            return ((ModbusTcpClientMessage) socketMessage).setEquipCode(ctx.channel().id().asShortText());
        } else {
            throw new ClientProtocolException("报文类型必须是["+ModbusTcpClientMessage.class.getSimpleName()+"]或其子类");
        }
    }

    @Override
    public ModbusTcpClientMessage createMessage(byte[] message) {
        return new ModbusTcpClientMessage(message);
    }

    @Override
    public String getName() {
        return "ModbusTcpClient";
    }

    @Override
    public String getDesc() {
        return DESC;
    }

    /**
     * modbus tcp 客户端得所有协议都是客户端主动协议
     * @see #getProtocol(ClientMessage)
     * @param message
     * @param type
     * @return
     */
    @Override
    protected ServerInitiativeProtocol instanceProtocol(M message, ProtocolType type) {
        return null;
    }

}
