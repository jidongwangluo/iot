package com.iteaj.iot.client.mqtt.impl;

import com.iteaj.iot.client.mqtt.MqttConnectProperties;
import io.netty.handler.codec.mqtt.MqttTopicSubscription;
import org.springframework.boot.context.properties.ConfigurationProperties;

import java.util.ArrayList;
import java.util.List;

@ConfigurationProperties(prefix = "iot.mqtt.default")
public class DefaultMqttConnectProperties extends MqttConnectProperties {

    private MqttListener listener;
    private List<MqttTopicSubscription> topics;
    public final static String DEFAULT_CLIENT = "IOT:DEFAULT:CLIENT:ID";

    public DefaultMqttConnectProperties() {
        super(DEFAULT_CLIENT);
    }

    public DefaultMqttConnectProperties(String clientId, MqttListener listener) {
        this(clientId, new ArrayList<>(), listener);
    }

    public DefaultMqttConnectProperties(String clientId, List<MqttTopicSubscription> topics, MqttListener listener) {
        super(clientId);
        this.topics = topics;
        this.listener = listener;
    }

    public DefaultMqttConnectProperties(String remoteHost, String clientId, MqttListener listener) {
        this(remoteHost, clientId, new ArrayList<>(), listener);
    }

    public DefaultMqttConnectProperties(String remoteHost, String clientId, List<MqttTopicSubscription> topics, MqttListener listener) {
        super(remoteHost, clientId);
        this.topics = topics;
        this.listener = listener;
    }

    public DefaultMqttConnectProperties(String remoteHost, Integer remotePort, String clientId, MqttListener listener) {
        this(remoteHost, remotePort, clientId, new ArrayList<>(), listener);
    }

    public DefaultMqttConnectProperties(String remoteHost, Integer remotePort, String clientId, List<MqttTopicSubscription> topics, MqttListener listener) {
        super(remoteHost, remotePort, clientId);
        this.topics = topics;
        this.listener = listener;
    }

    public MqttListener getListener() {
        return listener;
    }

    public void setListener(MqttListener listener) {
        this.listener = listener;
    }

    public List<MqttTopicSubscription> getTopics() {
        return topics;
    }

    public void setTopics(List<MqttTopicSubscription> topics) {
        this.topics = topics;
    }
}
