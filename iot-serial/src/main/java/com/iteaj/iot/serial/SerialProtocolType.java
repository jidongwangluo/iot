package com.iteaj.iot.serial;

import com.iteaj.iot.ProtocolType;

public enum SerialProtocolType implements ProtocolType {
    Event("串口事件");

    private String desc;

    SerialProtocolType(String desc) {
        this.desc = desc;
    }

    @Override
    public Enum getType() {
        return this;
    }

    @Override
    public String getDesc() {
        return this.desc;
    }
}
