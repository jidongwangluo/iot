package com.iteaj.iot.server;

import com.iteaj.iot.config.ConnectProperties;

public abstract class AbstractTcpServer implements IotSocketServer {

    private ConnectProperties config;

    public AbstractTcpServer(ConnectProperties serverConfig) {
        this.config = serverConfig;
    }

    @Override
    public ConnectProperties config() {
        return this.config;
    }

}
