package com.iteaj.iot.server;

import com.iteaj.iot.*;
import com.iteaj.iot.codec.SocketMessageDecoder;
import com.iteaj.iot.codec.filter.CombinedFilter;
import com.iteaj.iot.config.ConnectProperties;
import com.iteaj.iot.server.manager.DevicePipelineManager;
import com.iteaj.iot.server.protocol.ClientInitiativeProtocol;
import io.netty.channel.Channel;
import io.netty.channel.ChannelFuture;
import io.netty.channel.ChannelPipeline;
import io.netty.util.ReferenceCounted;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.GenericTypeResolver;

import java.lang.reflect.Constructor;
import java.util.Optional;

/**
 * 用来封装需要监听在TCP端口的设备的所需要的服务端组件
 */
public abstract class SocketServerComponent<M extends ServerMessage, R extends ReferenceCounted> extends ProtocolFactory<M>
        implements FrameworkComponent, IotSocketServer, SocketMessageDecoder<R> {

    private long startTime;
    /**
     * 如果设备编号重复 是否覆改掉上一台
     */
    private boolean override;
    private Class<M> messageClass;
    private Constructor<M> constructor;
    private DeviceManager deviceManager;
    private IotThreadManager threadManager;
    private ConnectProperties connectProperties;

    public SocketServerComponent(ConnectProperties connectProperties) {
        this.override = true;
        this.connectProperties = connectProperties;
        this.setDelegation(protocolTimeoutStorage());
    }

    @Override
    public void finished() {
        FrameworkComponent.super.finished();
        this.startTime = System.currentTimeMillis();
    }

    @Override
    public ConnectProperties config() {
        return this.connectProperties;
    }

    @Override
    protected abstract ClientInitiativeProtocol<M> instanceProtocol(M message, ProtocolType type);

    /**
     * 写出报文
     * @param equipCode 设备编号
     * @param msg 发送的协议
     * @param args 自定义参数
     * @return
     */
    public Optional<ChannelFuture> writeAndFlush(String equipCode, Object msg, Object... args) {
        return getDeviceManager().writeAndFlush(equipCode, msg, args);
    }

    /**
     * 写出协议
     * @see Protocol#requestMessage() 请求的报文
     * @see Protocol#responseMessage() 响应的报文
     * @param equipCode 设备编号
     * @param protocol 要写出的协议
     * @return
     */
    public Optional<ChannelFuture> writeAndFlush(String equipCode, Protocol protocol) {
        return this.writeAndFlush(equipCode, protocol, null);
    }

    public DeviceManager getDeviceManager() {
        return this.deviceManager;
    }

    /**
     * 连接通道是否已经关闭
     * @param equipCode
     * @return
     */
    public boolean isClose(String equipCode) {
        Channel channel = this.getDeviceManager().find(equipCode);
        if(channel == null) {
            return true;
        } else {
            return !channel.isActive();
        }
    }

    /**
     * 关闭指定设备编号的连接
     * @param equipCode
     * @return
     */
    public Optional<ChannelFuture> close(String equipCode) {
        return Optional.ofNullable(DevicePipelineManager.close(equipCode, getMessageClass()));
    }

    /**
     * 创建协议工厂
     * @return
     */
    public IotProtocolFactory protocolFactory() {
        return this;
    }

    @Override
    public void init(Object... args) {
        this.deviceManager = createDeviceManager();
        this.doInitChannel((ChannelPipeline) args[0]);
    }

    /**
     * 创建设备管理器
     * @return
     */
    protected DeviceManager createDeviceManager() {
        return new DevicePipelineManager(getName(), threadManager.getDeviceManageEventExecutor());
    }

    @Override
    public M createMessage(byte[] message) {
        return BeanUtils.instantiateClass(this.resolveConstructor(), message);
    }

    @Override
    public boolean isDecoder(Channel channel, ReferenceCounted msg) {
        return getFilter().orElse(CombinedFilter.DEFAULT).isDecoder(channel, msg);
    }

    @Override
    public Optional<CombinedFilter> getFilter() {
        return FilterManager.getInstance().getFilter(getClass());
    }

    /**
     * 设备服务端使用的报文类
     * 此类将用来标识唯一的设备服务组件
     * @return
     */
    @Override
    public Class<M> getMessageClass() {
        if(this.messageClass == null) {
            this.messageClass = (Class<M>) GenericTypeResolver
                    .resolveTypeArguments(getClass(), SocketServerComponent.class)[0];

            if(this.messageClass == null) {
                throw new IllegalArgumentException("为指定泛型["+getClass().getSimpleName()+"<M>]");
            }

            this.resolveConstructor();
        }

        return this.messageClass;
    }

    private Constructor<M> resolveConstructor() {
        if(this.constructor == null) {
            try {
                this.constructor = this.getMessageClass().getConstructor(byte[].class);
            } catch (NoSuchMethodException e) {
                final String simpleName = this.getMessageClass().getSimpleName();
                throw new ProtocolException("报文类型缺少构造函数["+simpleName+"(byte[])]", e);
            }
        }

        return this.constructor;
    }

    public final ProtocolTimeoutStorage protocolTimeoutStorage() {
        if(getDelegation() != null) {
            return this.getDelegation();
        }

        return doCreateProtocolTimeoutStorage();
    }

    /**
     * 创建超时管理器
     * @return
     */
    protected ProtocolTimeoutStorage doCreateProtocolTimeoutStorage() {
        return new ProtocolTimeoutStorage(getName());
    }

    @Autowired
    protected void setThreadManager(IotThreadManager threadManager) {
        this.threadManager = threadManager;
    }

    @Override
    public long startTime() {
        return startTime;
    }

    /**
     * 覆盖上一台相同设备编号的设备
     * @return
     */
    public boolean isOverride() {
        return override;
    }

    public void setOverride(boolean override) {
        this.override = override;
    }
}
