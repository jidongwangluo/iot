package com.iteaj.iot.server.component;

import com.iteaj.iot.AbstractProtocol;
import com.iteaj.iot.Protocol;
import com.iteaj.iot.ProtocolFactoryDelegation;
import com.iteaj.iot.config.ConnectProperties;
import com.iteaj.iot.server.ServerMessage;
import com.iteaj.iot.server.TcpServerComponent;
import io.netty.channel.ChannelPipeline;

/**
 * create time: 2021/2/20
 *
 * @author iteaj
 * @since 1.0
 */
public abstract class TcpDecoderServerComponent<M extends ServerMessage> extends TcpServerComponent<M> {

    private ProtocolFactoryDelegation delegation;

    public TcpDecoderServerComponent(ConnectProperties connectProperties) {
        super(connectProperties);
        this.delegation = new ProtocolFactoryDelegation(this, protocolTimeoutStorage());
    }

    @Override
    public abstract String getName();

}
