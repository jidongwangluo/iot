package com.iteaj.iot.server.component.impl;

import com.iteaj.iot.AbstractProtocol;
import com.iteaj.iot.ProtocolHandle;
import com.iteaj.iot.business.BusinessFactory;
import com.iteaj.iot.server.ServerSocketProtocol;

public class DefaultSimpleChannelProtocol extends ServerSocketProtocol<DefaultSimpleServerMessage> {

    @Override
    public AbstractProtocol buildRequestMessage() {
        return null;
    }

    @Override
    public AbstractProtocol buildResponseMessage() {
        return null;
    }

    @Override
    public AbstractProtocol exec(BusinessFactory factory) {
        return null;
    }

    @Override
    public AbstractProtocol exec(ProtocolHandle handle) {
        return null;
    }

    @Override
    public <T> T protocolType() {
        return null;
    }
}
