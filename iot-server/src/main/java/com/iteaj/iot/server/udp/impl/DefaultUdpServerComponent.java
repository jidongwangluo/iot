package com.iteaj.iot.server.udp.impl;

import com.iteaj.iot.AbstractProtocol;
import com.iteaj.iot.ProtocolType;
import com.iteaj.iot.config.ConnectProperties;
import com.iteaj.iot.server.component.DatagramPacketDecoderServerComponent;
import com.iteaj.iot.server.protocol.ClientInitiativeProtocol;

public class DefaultUdpServerComponent extends DatagramPacketDecoderServerComponent<DefaultUdpServerMessage> {

    public DefaultUdpServerComponent(ConnectProperties config) {
        super(config);
    }

    @Override
    public String getDesc() {
        return "UDP协议IOT默认实现";
    }

    @Override
    public AbstractProtocol getProtocol(DefaultUdpServerMessage message) {
        return new DefaultUdpServerProtocol(message);
    }

    @Override
    protected ClientInitiativeProtocol<DefaultUdpServerMessage> instanceProtocol(DefaultUdpServerMessage message, ProtocolType type) {
        return null;
    }

    @Override
    public String getName() {
        return "UDP(默认)";
    }

    @Override
    public Class<DefaultUdpServerMessage> getMessageClass() {
        return DefaultUdpServerMessage.class;
    }

    @Override
    public DefaultUdpServerMessage createMessage(byte[] message) {
        return new DefaultUdpServerMessage(message);
    }
}
