package com.iteaj.iot.test.mqtt;

import com.iteaj.iot.Message;
import com.iteaj.iot.client.ClientProtocolHandle;
import com.iteaj.iot.client.mqtt.MqttClient;
import com.iteaj.iot.client.mqtt.MqttConnectProperties;
import com.iteaj.iot.client.mqtt.impl.*;
import com.iteaj.iot.consts.ExecStatus;
import com.iteaj.iot.test.IotTestHandle;
import com.iteaj.iot.test.IotTestProperties;
import com.iteaj.iot.test.TestConst;
import io.netty.channel.ChannelFuture;
import io.netty.handler.codec.mqtt.MqttQoS;
import io.netty.handler.codec.mqtt.MqttTopicSubscription;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.concurrent.ThreadPoolTaskScheduler;

import java.nio.charset.StandardCharsets;
import java.time.Duration;
import java.time.Instant;
import java.util.Arrays;
import java.util.concurrent.TimeUnit;

/**
 * create time: 2021/9/3
 *
 * @author iteaj
 * @since 1.0
 */
public class MqttClientTestHandle implements ClientProtocolHandle<MqttPublishTestProtocol>, IotTestHandle {

    @Autowired
    private IotTestProperties properties;
    @Autowired
    private ThreadPoolTaskScheduler scheduler;
    @Autowired
    private MqttClientTestComponent component;
    @Autowired(required = false)
    private DefaultMqttComponent defaultMqttComponent;
    private Logger logger = LoggerFactory.getLogger(getClass());

    public static final String TOPIC_RESPONSE = "iteaj/test/cus/response";
    public static final String AT_MOST_ONCE_TOPIC = "iteaj/test/iot/atMostOnce/0";
    public static final String EXACTLY_ONCE_TOPIC = "iteaj/test/iot/exactlyOnce/2";
    public static final String AT_LEAST_ONCE_TOPIC = "iteaj/test/iot/atLeastOnce/1";

    @Override
    public Object handle(MqttPublishTestProtocol protocol) {
        return null;
    }

    @Override
    public void start() throws Exception{
        IotTestProperties.TestMqttConnectProperties config = properties.getMqtt();

        if(defaultMqttComponent != null) {
            String subscriptionClientId = "Iot:Client:Listener";
            defaultMqttComponent.createNewClientAndConnect(new DefaultMqttConnectProperties(config.getHost()
                    , config.getPort(), subscriptionClientId, protocol -> {

                DefaultMqttConnectProperties properties = protocol.requestMessage().getProperties();
                byte[] message = protocol.requestMessage().getMessage();
                String value = new String(message);
                if(value.equals("iteaj/subscription/create")) {
                    logger.info(TestConst.LOGGER_PROTOCOL_FUNC_DESC, defaultMqttComponent.getName()
                            , "createNewClientAndConnect(DefaultMqttConnectProperties)", properties.getClientId(), "通过" );
                    defaultMqttComponent.unsubscribe(subscriptionClientId, "iteaj/subscription/create/#").addListener(future -> {
                        if(future.isSuccess()) {
                            new DefaultMqttPublishProtocol("iteaj/unsubscription/create".getBytes(StandardCharsets.UTF_8), "iteaj/subscription/create/68").request();
                        }
                    });
                } else if(value.equals("iteaj/subscription/newAdd")) {
                    logger.info(TestConst.LOGGER_PROTOCOL_FUNC_DESC, defaultMqttComponent.getName()
                            , "subscribe", properties.getClientId(), "通过" );
                    defaultMqttComponent.unsubscribe(subscriptionClientId, "iteaj/subscription/newAdd/#").addListener(future -> {
                        if(future.isSuccess()) {
                            new DefaultMqttPublishProtocol("iteaj/unsubscription/newAdd".getBytes(StandardCharsets.UTF_8), "iteaj/subscription/newAdd/68").request();
                        }
                    });
                } else {
                    logger.error(TestConst.LOGGER_PROTOCOL_FUNC_DESC, defaultMqttComponent.getName(), value, properties.getClientId(), "失败" );
                }
                // 取消订阅
            }));

            scheduler.schedule(() -> {
                // subscribe测试
                defaultMqttComponent.subscribe(subscriptionClientId, "iteaj/subscription/create/#", MqttQoS.AT_LEAST_ONCE);
                defaultMqttComponent.subscribe(subscriptionClientId, "iteaj/subscription/newAdd/#", MqttQoS.AT_LEAST_ONCE);

                new DefaultMqttPublishProtocol("iteaj/subscription/create".getBytes(StandardCharsets.UTF_8), "iteaj/subscription/create/68").request();
                new DefaultMqttPublishProtocol("iteaj/subscription/newAdd".getBytes(StandardCharsets.UTF_8), "iteaj/subscription/newAdd/68").request();
            }, Instant.now().plusSeconds(20));
        }

        IotTestProperties.TestMqttConnectProperties willTopicConfig = new IotTestProperties.TestMqttConnectProperties();
        // 不保留遗嘱的测试客户端
        BeanUtils.copyProperties(config, willTopicConfig, "clientId", "deviceSn");
        willTopicConfig.setWillTopic("iteaj/willTopic/iot/"+willTopicConfig.getClientId());
        willTopicConfig.setWillRetain(false); // 不保留的遗嘱
        willTopicConfig.setWillQos(MqttQoS.AT_LEAST_ONCE);
        willTopicConfig.setWillMessage("{\"retain\": false}"); // 不保留遗嘱测试
        MqttClient retainWillClient = component.createNewClientAndConnect(willTopicConfig);

        MqttConnectProperties willRetainTopicConfig = new IotTestProperties.TestMqttConnectProperties();
        // 保留遗嘱的测试客户端
        BeanUtils.copyProperties(config, willRetainTopicConfig, "clientId", "deviceSn");
        willRetainTopicConfig.setWillTopic("iteaj/willTopic/iot/"+willRetainTopicConfig.getClientId());
        willRetainTopicConfig.setWillRetain(true); // 保留的遗嘱
        willRetainTopicConfig.setWillQos(MqttQoS.AT_MOST_ONCE);
        willRetainTopicConfig.setWillMessage("{\"retain\": true}"); // 保留遗嘱测试
        MqttClient willClient = component.createNewClientAndConnect(willRetainTopicConfig);

        System.out.println("---------------------------------------------------- 开始mqtt测试 ----------------------------------------------------------");

        willClient.disconnect(true); // 断线移除测试
        retainWillClient.disconnect(false); // 断线重连测试

        // 测试最多发送一次报文
        new MqttPublishTestProtocol(MqttQoS.AT_MOST_ONCE, AT_MOST_ONCE_TOPIC, willTopicConfig.getDeviceSn()).request(willTopicConfig);

        // 最少发送一次报文
        new MqttPublishTestProtocol(MqttQoS.AT_LEAST_ONCE, AT_LEAST_ONCE_TOPIC, willTopicConfig.getDeviceSn()).request(willTopicConfig);

        // 确保一定发送一次
        new MqttPublishTestProtocol(MqttQoS.EXACTLY_ONCE, EXACTLY_ONCE_TOPIC, willTopicConfig.getDeviceSn()).request(willTopicConfig);

        TimeUnit.SECONDS.sleep(3);
    }

    @Override
    public int getOrder() {
        return 1000 * 30;
    }
}
