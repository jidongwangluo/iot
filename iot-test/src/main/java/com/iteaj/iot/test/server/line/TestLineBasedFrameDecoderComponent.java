package com.iteaj.iot.test.server.line;

import com.iteaj.iot.AbstractProtocol;
import com.iteaj.iot.ProtocolType;
import com.iteaj.iot.config.ConnectProperties;
import com.iteaj.iot.server.component.LineBasedFrameDecoderServerComponent;
import com.iteaj.iot.server.protocol.ClientInitiativeProtocol;
import com.iteaj.iot.server.protocol.HeartbeatProtocol;
import com.iteaj.iot.test.TestProtocolType;
import com.iteaj.iot.test.message.TMessageHead;
import com.iteaj.iot.test.message.line.LineMessage;
import com.iteaj.iot.test.server.line.protocol.LineClientInitiativeProtocol;

/**
 * 测试基于换行符的解码器 + 网关组件
 * @see LineBasedFrameDecoderServerComponent
 */
public class TestLineBasedFrameDecoderComponent extends LineBasedFrameDecoderServerComponent<LineMessage> {

    public TestLineBasedFrameDecoderComponent(ConnectProperties connectProperties) {
        super(connectProperties, 5 * 1024);
    }

    @Override
    public AbstractProtocol getProtocol(LineMessage message) {
        TMessageHead head = message.getHead();
        // 心跳报文
        if(head.getType() == TestProtocolType.Heart) {
            return HeartbeatProtocol.getInstance(message);

            // 此请求属于客户端响应平台
        } else  if(head.getType() == TestProtocolType.PIReq) {
            return remove(head.getMessageId());

            // 客户端主动请求平台的协议
        } else {
            return new LineClientInitiativeProtocol(message);
        }
    }

    @Override
    protected ClientInitiativeProtocol<LineMessage> instanceProtocol(LineMessage message, ProtocolType type) {
        return null;
    }

    @Override
    public String getDesc() {
        return "测试服务端换行符解码器";
    }

    @Override
    public String getName() {
        return "换行符解码";
    }

}
