package com.iteaj.iot.taos;

import com.iteaj.iot.taos.proxy.TaosHandleProxyMatcher;
import org.springframework.beans.factory.BeanCreationException;
import org.springframework.beans.factory.BeanFactory;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.core.annotation.Order;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.scheduling.annotation.EnableScheduling;

import javax.sql.DataSource;

/**
 * create time: 2022/1/18
 *
 * @author iteaj
 * @since 1.0
 */
@EnableScheduling
@EnableConfigurationProperties(IotTaosProperties.class)
public class IotTaosAutoConfiguration {

    @Bean("taosJdbcTemplate")
    @ConditionalOnMissingBean(name = "taosJdbcTemplate")
    public JdbcTemplate taosJdbcTemplate(@Qualifier("taosDataSource") DataSource taosDataSource) {
        return IotTaos.databaseSource.taosJdbcTemplate = new JdbcTemplate(taosDataSource);
    }

    @Bean("taosDataSource")
    @ConditionalOnMissingBean(name = "taosDataSource")
    public DataSource taosDataSource(IotTaosProperties properties) {
        if(properties.getDatasource() == null) {
            throw new BeanCreationException("未配置数据源[iot.taos.datasource.xx]");
        }

        if(properties.getDatasource().getType() == null) {
            throw new BeanCreationException("未指定数据源类型[iot.taos.datasource.type]");
        }

        return properties.getDatasource().build();
    }

    @Bean
    @ConditionalOnMissingBean(TaosSqlManager.class)
    public TaosSqlManager taosSqlManager(BeanFactory beanFactory
            , @Qualifier("taosJdbcTemplate") JdbcTemplate taosJdbcTemplate) {
        return new DefaultTaosSqlManager(beanFactory, taosJdbcTemplate);
    }

    @Bean
    @Order(10000)
    @ConditionalOnBean(TaosHandle.class)
    @ConditionalOnMissingBean(TaosHandleProxyMatcher.class)
    public TaosHandleProxyMatcher taosHandleProxyMatcher(TaosSqlManager taosSqlManager) {
        return new TaosHandleProxyMatcher(taosSqlManager);
    }

}
