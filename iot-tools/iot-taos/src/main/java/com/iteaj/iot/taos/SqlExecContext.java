package com.iteaj.iot.taos;

import org.springframework.jdbc.core.SqlParameterValue;

public class SqlExecContext {

    private StringBuilder sql;

    private SqlParameterValue[] values;


    public SqlExecContext(StringBuilder sql, SqlParameterValue[] values) {
        this.sql = sql;
        this.values = values;
    }

    public StringBuilder getSql() {
        return sql;
    }

    public void setSql(StringBuilder sql) {
        this.sql = sql;
    }

    public SqlParameterValue[] getValues() {
        return values;
    }

    public void setValues(SqlParameterValue[] values) {
        this.values = values;
    }
}
